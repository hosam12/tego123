<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class EditCenter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'titlear'=>'required|string|min:3',
            'titleen'=>'required|string|min:3',
             'describear'=>'required',
             'describeen'=>'required',
            'main_image'=>'image',
        ];
    }

       public function messages()
    {


        return [
            'titlear.required'=>'عنوان الخبر -عربي- فارغ',
            'titlear.min'=>'أحرف العنوان -عربي- أقل من 3',
              'titleen.required'=>'عنوان الخبر -إنجليزي- فارغ',
            'titleen.min'=>'أحرف العنوان -إنجليزي- أقل من 3',
            'main_image.image'=>'يجب أن تكون صورة وليست ملف',
            'describear.required'=>'الوصف -عربي- فارغ',
             'describeen.required'=>'الوصف -إنجليزي- فارغ',


        ];
        // else
        // return[

        //       'titlear.required'=>'The news title (Ar) is empty',
        //     'titleen.required'=>'The news title (En) is empty',

        //     'titlear.min'=>'Title characters (Ar)  less than 3',
        //     'titleen.min'=>'Title characters (En) less than 3',

        //     'main_image.image'=>'It should be an image, not a file',
        //     'describear.required'=>'Description (Ar) is empty',
        //     'describeen.required'=>'Description (En) is empty',



        // ];
    }
}
