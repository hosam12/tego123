<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use phpDocumentor\Reflection\PseudoTypes\True_;

class CreateBrid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'countbrid'=>'nullable|integer|',
            'countplant'=>'nullable|integer',
            'countmammal'=>'nullable|integer',
        ];
    }

      public function messages()
    {



        return [
        'countmammal.integer'=>'عدد الثديات يجب أن يكون رقم',
          'countplant.integer'=>'عدد الفصائل النباتية يجب أن يكون رقم',
            'countbrid.integer'=>'عدد الطيور يجب أ يكن رقم',

        ];





    }
}
