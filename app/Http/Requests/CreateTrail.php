<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTrail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'namear'=>'required|string',
            'nameen'=>'required|string',
            'descar'=>'required|string',
            'descen'=>'required|string',
            'main_image'=>'required|image',
            'urlvid'=>'required|string',
            'urlimage'=>'required|string',
        ];
    }
      public function messages(){
    return [

      'namear.required'=>'اسم المسار - عربي فارغ',
          'nameen.required'=>'اسم المسار - إنجليزي- فارغ',
          'descar'=>'التفاصيل - عربي- فارغ',
          'descen.required'=>'التفاصيل - إنجليزي - فارغ',
          'main_image.required'=>'الصورة الرئيسية فارغة',
          'urlvid.required'=>'رابط الفبدبو فارغ',
          'urlimage'=>'رابط الصورة 360 فارغ',
    ];

      }

}
