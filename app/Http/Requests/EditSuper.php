<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class EditSuper extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:supers,email,',
            'password'=>'required|string|min:6',
            'confirm'=>'same:password',
            'image'=>'image',
        ];
    }

     public function messages()
    {



        return[
        'name.required'=>'الإسم فارغ',
         'email.required'=>'الإيميل فارغ',
         'email.email'=>'الرجاء التأكد من الإيميل',
         'email.unique'=>'الإيميل مُكرر',
         'password.required'=>'كلمة السر فارغة',
         'password.min'=>'كلمة السر أقل من 6 حروف',
         'confirm.same'=>'كلمتا السر غير متطابقتان',
         'image.image'=>'الرجاء رفع صورة'
        ];
    }




}
