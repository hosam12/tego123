<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class CreateSuper extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:supers,email',
            'password'=>'required|string|min:6',
            'confirm'=>'same:password',
            'image'=>'image',
        ];
    }

     public function messages()
    {
        //  if(App::getLocale()=='ar'){


        return[
        'name.required'=>'الإسم فارغ',
         'email.required'=>'الإيميل فارغ',
         'email.email'=>'الرجاء التأكد من الإيميل',
         'email.unique'=>'الإيميل مُكرر',
         'password.required'=>'كلمة السر فارغة',
         'password.min'=>'كلمة السر أقل من 6 حروف',
         'confirm.same'=>'كلمتا السر غير متطابقتان',
         'image.image'=>'الرجاء رفع صورة'
        ];
    // }
    // else {
    //        return[
    //     'name.required'=>'The  Name Empty',
    //      'email.required'=>'The Email Empty',
    //      'email.email'=>'Please enter from the e-mail',
    //      'email.unique'=>'The email is duplicate',
    //      'password.required'=>'Password is empty',
    //      'password.min'=>'Password is less than 6 characters long',
    //      'confirm.same'=>'Password does not match',
    //      'image.image'=> 'Please upload a picture'
    //     ];
    // }
    }

}
