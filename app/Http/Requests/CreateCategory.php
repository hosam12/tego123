<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class CreateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {




        return [
            'name'=>'required|string',
            'image'=>'required|image',
            'status'=>'string|in:on',
        ];

    }

     public function messages()
    {
        return [
            'name.required'=>'إسم الفئة فارغ',
            'image.required'=>'لا يوجد صورة',
            'image.image'=>'يجب أن تكون صورة وليست ملف',


        ];


    }


}
