<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class CreateVillage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'namear'=>'required|string|min:3',
            'nameen'=>'required|string|min:3',
            'descar'=>'required|string',
            'descen'=>'required|string',
            'image'=>'required|image',
            'status'=>'in:on',
        ];

    }

     public function messages()
    {


        return [
            'nameen.required'=>'الإسم- إنجليزي- فارغ',
            'descen.required'=>'الوصف-إنجليزي -  فارغ',
             'namear.required'=>'الإسم- عربي -فارغ',
            'descar.required'=>'الوصف - عربي - فارغ',
            'nameen.min'=>'أحرف الإسم -إنجليزي-أقل من 3',
             'namear.min'=>'أحرف الإسم -عربي-أقل من 3',
            'image.required'=>'لا يوجد صورة',
            'image.image'=>'يجب أن تكون صورة وليست ملف',


        ];




    }
}
