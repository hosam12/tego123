<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use phpDocumentor\Reflection\PseudoTypes\True_;

class CreateCenter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titlear'=>'required|string|min:3',
            'titleen'=>'required|string|min:3',
            'main_image'=>'required|image',
            'describear'=>'required|string',
             'describeen'=>'required|string',
        ];
    }


      public function messages()
    {


        return [
            'titlear.required'=>'عنوان الخبر -عربي-  فارغ',
            'titlear.min'=>'أحرف العنوان -عربي- أقل من 3',
            'titleen.required'=>'عنوان الخبر -إنجليزي- فارغ',
            'titleen.min'=>'أحرف العنوان -إنجليزي- أقل من 3',
            'main_image.required'=>'لا يوجد صورة',
            'main_image.image'=>'يجب أن تكون صورة وليست ملف',
            'describeen.required'=>'الوصف -إنجليزي- فارغ',
            'describear.required'=>'الوصف -عربي- فارغ',


        ];





    }

    // public function getmessage($error){

    // }

}
