<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTrail;
use App\Http\Requests\EditTrail;
use App\Models\Image;
use App\Models\Trail;
use Illuminate\Http\Request;

class TrailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trails=Trail::with('images')->get();
        return view('cms.admin.trails.index',['trails'=>$trails]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.admin.trails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTrail $request)
    {
        $trail=new Trail();
        $trail->namear=$request->get('namear');
         $trail->nameen=$request->get('nameen');
         $trail->descar=$request->get('descar');
            $trail->descaen=$request->get('descen');
            $trail->urlvid=$request->get('urlvid');
            $trail->urlimage=$request->get('urlimage');

               if ($request->hasFile('main_image')) {
            $imagefile = $request->file('main_image');
            $imagename = time() . 'center' . 'trails' . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/trails', $imagename);
            $trail->image = $imagename;
        }

        $save=$trail->save();
        if($save){


                foreach ($request->file('image',[]) as $im) {

          $h= new Image();
          $imagefile=$im;
          $imagename=time().'_'.'store'.'_'.'_'.$imagefile->getClientOriginalName();
          $h->image=$imagename;

          $imagefile->move('images/trails',$imagename);
          $h->object_type='App\Category';
          $h->object_id=1;
        $trail->images()->save($h);



          }
                SuccessError::Success('تم التعديل بنجاح');
                return redirect()->back();


        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trail  $trail
     * @return \Illuminate\Http\Response
     */
    public function show(Trail $trail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trail  $trail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trail=Trail::with('images')->where('id',$id)->first();
        return view('cms.admin.trails.edit',['trail'=>$trail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trail  $trail
     * @return \Illuminate\Http\Response
     */
    public function update(EditTrail $request,$id)
    {
        //
             $trail=Trail::find($id);
        $trail->namear=$request->get('namear');
         $trail->nameen=$request->get('nameen');
         $trail->descar=$request->get('descar');
            $trail->descaen=$request->get('descen');
            $trail->urlvid=$request->get('urlvid');
            $trail->urlimage=$request->get('urlimage');

               if ($request->hasFile('main_image')) {
            $imagefile = $request->file('main_image');
            $imagename = time() . 'center' . 'trails' . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/trails', $imagename);
            $trail->image = $imagename;
        }

        $save=$trail->save();
        if($save){


                foreach ($request->file('image',[]) as $im) {

          $h= new Image();
          $imagefile=$im;
          $imagename=time().'_'.'store'.'_'.'_'.$imagefile->getClientOriginalName();
          $h->image=$imagename;

          $imagefile->move('images/trails',$imagename);
          $h->object_type='App\Category';
          $h->object_id=1;
        $trail->images()->save($h);



          }
                SuccessError::Success('تم التعديل بنجاح');
                return redirect()->back();


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trail  $trail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trail $trail)
    {
        //
    }
}
