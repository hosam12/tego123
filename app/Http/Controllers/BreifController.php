<?php

namespace App\Http\Controllers;

use App\Models\Breif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BreifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $breif = Breif::first();
        return view('cms.admin.breifs.index', ['breif' => $breif]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $breif = Breif::first();
            if(!$breif){
            $breif = new Breif();

            }

        $breif->descen = $request->get('descen');
         $breif->titleen = $request->get('titleen');
           $breif->descar = $request->get('descar');
         $breif->titlear = $request->get('titlear');
            if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().'breif'.'image'.'Breif'.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/breifs',$imagename);
         $breif->image=$imagename ;
         }

         $save=$breif->save();
        if ($save) {

            $lang = App::getLocale();
            if ($lang == 'ar') {
                SuccessError::Success('تم الإنشاء بنجاح');
                return redirect()->back();
            } else {
                SuccessError::Success('Created successfully');
                return redirect()->back();

            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Breif  $breif
     * @return \Illuminate\Http\Response
     */
    public function show(Breif $breif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Breif  $breif
     * @return \Illuminate\Http\Response
     */
    public function edit(Breif $breif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Breif  $breif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Breif $breif)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Breif  $breif
     * @return \Illuminate\Http\Response
     */
    public function destroy(Breif $breif)
    {
        //
    }
}
