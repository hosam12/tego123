<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBrid;
use App\Models\Brid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.admin.brids.index',['brid'=>Brid::first()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBrid $request)
    {

        $brid=Brid::first();
        if(!$brid){
            $brid=new Brid();
        }
        $brid->descar=$request->get('descar');
        $brid->descen=$request->get('descen');
        $brid->titlear=$request->get('titlear');
        $brid->titleen->$request->get('titleen');
        $brid->countplant=$request->get('countplant');
        $brid->countmammal=$request->get('countmammal');
        $brid->countbrid=$request->get('countbrid');
        $save=$brid->save();
       if ($save) {
           $lang = App::getLocale();
            if ($lang == 'ar') {
                SuccessError::Success('تم التعديل بنجاح');
                return redirect()->back();
            } else {
                SuccessError::Success('Edit successfully');
                return redirect()->back();

        }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brid  $brid
     * @return \Illuminate\Http\Response
     */
    public function show(Brid $brid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brid  $brid
     * @return \Illuminate\Http\Response
     */
    public function edit(Brid $brid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brid  $brid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brid $brid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brid  $brid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brid $brid)
    {
        //
    }
}
