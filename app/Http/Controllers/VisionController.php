<?php

namespace App\Http\Controllers;

use App\Models\Vision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class VisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vision = Vision::first();
        return view('cms.admin.visions.index', ['vision' => $vision]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'desc' => 'required',

        // ]);


            $vision = Vision::first();
            if(!$vision){
            $vision = new Vision();

            }

        $vision->visionar = $request->get('visionar');
         $vision->mymessagear = $request->get('mymessagear');
          $vision->visionen = $request->get('visionen');
         $vision->mymessageen = $request->get('mymessageen');
            if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().'vision'.'image'.'mymessage'.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/visions',$imagename);
         $vision->image=$imagename ;
         }

         $save=$vision->save();
        if ($save) {


                SuccessError::Success('تم الإنشاء بنجاح');
                return redirect()->back();



        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vision  $vision
     * @return \Illuminate\Http\Response
     */
    public function show(Vision $vision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vision  $vision
     * @return \Illuminate\Http\Response
     */
    public function edit(Vision $vision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vision  $vision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vision $vision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vision  $vision
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vision $vision)
    {
        //
    }
}
