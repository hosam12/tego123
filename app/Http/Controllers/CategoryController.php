<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('cms.admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategory $request)
    {
        // dd($request->h);
        $category = new Category();
        $category->name=$request->get('name');
          $token=Str::uuid();
        $category->describe=$request->get('describe');
          if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().'category'.$token.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/categories',$imagename);
         $category->image=$imagename ;
         }
        $category->status=$request->get('status')=='on'?'Visible':'InVisible';
        $save=$category->save();
        if($save){
            SuccessError::Success('تم إنشاء الفئة بنجاح');
            return redirect()->back();

        }
          else
            SuccessError::Error('فشلت العملية');
            return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
