<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Mail\AdminPasswordReset;
use App\Models\Super;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SuperAuthController extends Controller
{
    //
    // use AuthenticatesUsers;

    // protected $guardName = 'admin';
    // protected $maxAttempts = 4;
    // protected $decayMinutes = 2;

    public function __construct()
    {

        $this->middleware('guest:super')->except('logout');
        // $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginView(){
        return view('cms.auth.super.login');
    }

    public function login(Request $request){

        $request->validate([
            'email'=>'required|email|string|exists:supers,email',
            'password'=>'required|string|min:3|max:10',
            'remember_me'=>'string|in:on'
        ]);




        $rememberMe = $request->remember_me == 'on' ? true : false;

        $credentials = [
            'email'=>$request->email,
            'password'=>$request->password,
        ];
        // dd($request->passwor)


        if (Auth::guard('super')->attempt($credentials, $rememberMe)){
            // dd(10);

            $user = Auth::guard('super')->user();

            if ($user->status == "Active"){

                return redirect()->route('admin.dashbord','ar');
            }else{
                //  dd(54);
                Auth::guard('super')->logout();
                return redirect()->guest(route('super.blocked'));
            }
        }else{

            return redirect()->back()->withInput();
        }
    }
    public function blocked(){

        return view('cms.auth.super.blocked');
    }

    // public function showResetPasswordView(){
    //     return view('cms.admin.settings.reset_password');
    // }

    // public function resetPassword(Request $request)
    // {
    //     $request->validate([
    //         'current_password' => 'required|string|password:admin',
    //         'new_password' => 'required|string',
    //         'new_password_confirmation' => 'required|string|same:new_password'
    //     ],['current_password.password'=>'Your current password is not correct']);

    //     $user = Auth::user();
    //     $user->password = Hash::make($request->get('new_password'));
    //     $isSaved = $user->save();
    //     if ($isSaved) {
    //         return response()->json(['icon' => 'success', 'title' => 'Password changed successfully'], 200);
    //     } else {
    //         return response()->json(['icon' => 'success', 'title' => 'Password change failed!'], 400);
    //     }
    // }

    public function logout(Request $request){
        // dd(45);
        Auth::guard('super')->logout();
        $request->session()->invalidate();
        return redirect()->guest(route('super.login.view'));
    }

    // public function showForgetPassword()
    // {
    //     return view('cms.admin.auth.forgot-password');
    // }

    // public function requestNewPassword(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required|exists:admins,email|email',
    //     ], ['email.exists' => 'This is email is not registered before']);

    //     $newPassword = Str::random(8);

    //     $admin = Admin::where('email', $request->get('email'))->first();
    //     $admin->password = Hash::make($newPassword);
    //     $isSaved = $admin->save();
    //     if ($isSaved) {
    //         $this->sendResetPasswordEmail($admin, $newPassword);
    //         return redirect()->route('cms.admin.login_view');
    //     } else {

    //     }
    // }

    // private function sendResetPasswordEmail(Admin $admin, $newPassword)
    // {
    //     Mail::queue(new AdminPasswordReset($admin, $newPassword));
    // }
}
