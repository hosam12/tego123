<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVillage;
use App\Http\Requests\EditVillage;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $villages=Village::all();
        return view('cms.admin.villages.index',['villages'=>$villages]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.admin.villages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateVillage $request)
    {
        //
        // dd(45);
        $village=new Village();
        $village->nameen=$request->get('nameen');
  $village->namear=$request->get('namear');
           if ($request->hasFile('image')) {
            $imagefile = $request->file('image');
            $imagename = time() . 'center' . 'Village'. ' ' . ' ' .$imagefile->getClientOriginalName();

            $imagefile->move('images/villages', $imagename);
            $village->image = $imagename;
        }
        $village->descen=$request->get('descen');
           $village->descar=$request->get('descar');
        $village->status = $request->get('status') == 'on' ? 'Visible' : 'InVisible';

           $save=$village->save();
        if ($save) {



                SuccessError::Success('تم الإنشاء بنجاح');
                return redirect()->back();




        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $village=Village::find($id);
        return view('cms.admin.villages.show',['village'=>$village]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $village=Village::find($id);
        return view('cms.admin.villages.edit',['village'=>$village]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function update(EditVillage $request, $id)
    {
        //

           $village=Village::find($id);


           if ($request->hasFile('image')) {
            $imagefile = $request->file('image');
            $imagename = time() . 'center' . 'Village'. ' ' . ' ' .$imagefile->getClientOriginalName();

            $imagefile->move('images/villages', $imagename);
            $village->image = $imagename;
        }
        $village->descar=$request->get('descar');
        $village->descen=$request->get('descen');
        $village->namear=$request->get('namear');
        $village->nameen=$request->get('nameen');
        $village->status = $request->get('status') == 'on' ? 'Visible' : 'InVisible';

           $save=$village->save();
        if ($save) {



                SuccessError::Success('تم التعديل بنجاح');
                return redirect()->back();




        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Village  $village
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Village::destroy($id);

        if ($delete) {

             return response()->json(['icon' => 'success', 'title' => 'تم الحذف بنجاح  '], 200);






        }
    }
}
