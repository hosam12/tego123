<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCenter;
use App\Http\Requests\EditCenter;
use App\Models\Center;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class CenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $centers = Center::all();
        return view('cms.admin.centers.index', ['centers' => $centers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.admin.centers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCenter $request)
    {

        $center = new Center();
        $center->titleen = $request->get('titleen');
        $center->describeen = $request->get('describeen');
          $center->titlear = $request->get('titlear');
        $center->describear = $request->get('describear');
        $token = Str::uuid();

        if ($request->hasFile('main_image')) {
            $imagefile = $request->file('main_image');
            $imagename = time() . 'center' . $token . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/centers', $imagename);
            $center->main_image = $imagename;
        }
        $center->status = $request->get('status') == 'on' ? 'Visible' : 'InVisible';
        $save = $center->save();
        if ($save) {


                SuccessError::Success('تم الإنشاء بنجاح');
                return redirect()->back();





        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {

        // dd($id);
        $center = Center::find($id);
        return view('cms.admin.centers.show', ['center' => $center]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
        // dd($id);
        $center = Center::findOrFail($id);
        return view('cms.admin.centers.edit', ['center' => $center]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(EditCenter $request,  $id)
    {

        $center = Center::find($id);
        $center->titleen = $request->get('titleen');
        $center->describeen = $request->get('describeen');
          $center->titlear = $request->get('titlear');
        $center->describear = $request->get('describear');

        if ($request->hasFile('main_image')) {
            $token = Str::uuid();

            $imagefile = $request->file('main_image');
            $imagename = time() . 'center' . $token . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/centers', $imagename);
            $center->main_image = $imagename;
        }
        $center->status = $request->get('status') == 'on' ? 'Visible' : 'InVisible';
        $save = $center->save();
        if ($save) {

                SuccessError::Success('Edit successfully');
                return redirect()->back();



        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {

        $delete = Center::destroy($id);

          if ($delete) {

             return response()->json(['icon' => 'success', 'title' => 'تم الحذف بنجاح  '], 200);




        }

    }
}
