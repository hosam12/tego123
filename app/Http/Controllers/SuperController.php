<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuper;
use App\Models\Super;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class SuperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $super= new Super();
        // $super->name="gggg";
        // $super->email="h1@gmail.com";
        // $super->password=Hash::make('123456');
        // $super->save();
        // dd(4554);
        $supers=Super::all();
        return view('cms.admin.supers.index',['supers'=>$supers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.admin.supers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSuper $request)
    {
        //
        $super=new Super();
        $super->name=$request->get('name');
        $super->email=$request->get('email');
        $super->password=Hash::make(($request->get('password')));
        $super->status = $request->get('status') == 'on' ? 'Active' : 'InActive';
           if ($request->hasFile('image')) {
            $imagefile = $request->file('image');
            $imagename = time() . 'center' . 'Super' . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/supers', $imagename);
            $super->image = $imagename;
        }

              $save=$super->save();

            if ($save) {


                SuccessError::Success('تم الإنشاء بنجاح');
                return redirect()->back();





        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Super  $super
     * @return \Illuminate\Http\Response
     */
    public function show(Super $super)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Super  $super
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('cms.admin.supers.edit',['super'=>Super::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Super  $super
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

         $request->request->add(['id' => $id]);
        $request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:supers,email,' .$id,
            'image'=>'image',
        ],[

        'name.required'=>'الإسم فارغ',
         'email.required'=>'الإيميل فارغ',
         'email.email'=>'الرجاء التأكد من الإيميل',
         'email.unique'=>'الإيميل مُكرر',
         'image.image'=>'الرجاء رفع صورة'
        ]
    );


      $super=Super::find($id);
        $super->name=$request->get('name');
        $super->email=$request->get('email');
        $super->password=Hash::make(($request->get('password')));
        $super->status = $request->get('status') == 'on' ? 'Active' : 'InActive';
           if ($request->hasFile('image')) {
            $imagefile = $request->file('image');
            $imagename = time() . 'center' . 'Super' . ' ' . ' ' . $imagefile->getClientOriginalName();

            $imagefile->move('images/supers', $imagename);
            $super->image = $imagename;
        }

              $save=$super->save();

            if ($save) {


                SuccessError::Success('تم التعديل بنجاح');
                return redirect()->back();




        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Super  $super
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 1;
        //
    }
}
