<?php

use App\Http\Controllers\AreaController;
use App\Http\Controllers\Auth\SuperAuthController;
use App\Http\Controllers\BreifController;
use App\Http\Controllers\BridController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CenterController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\SuperController;
use App\Http\Controllers\VillageController;
use App\Http\Controllers\VisionController;
use App\Models\Area;
use App\Models\Super;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('cms/admin')->middleware('auth:super')->group(function () {

        Route::resource('vision', VisionController::class);
        Route::resource('village', VillageController::class)->middleware('auth:super');
        Route::resource('category',CategoryController::class)->middleware('auth:super');
        Route::resource('message',MessageController::class)->middleware('auth:super');
        Route::resource('breif',BreifController::class)->middleware('auth:super');
        Route::resource('area',AreaController::class)->middleware('auth:super');
        Route::resource('brid',BridController::class)->middleware('auth:super');
        Route::resource('super',SuperController::class)->middleware('auth:super');

        Route::resource('center',CenterController::class)->middleware('auth:super')->middleware('auth:super');
        Route::get('/',[DashbordController::class,'admin'])->name('admin.dashbord')->middleware('auth:super');
        Route::get('logout', [SuperAuthController::class,'logout'])->name('super.logout')->middleware('auth:super');

});

Route::prefix('cms/admin/')->namespace('Auth')->group(function () {

    Route::get('page/login', [SuperAuthController::class,'showLoginView'])->name('super.login.view');
    Route::post('login',  [SuperAuthController::class,'login'])->name('super.login.store');
    Route::get('blocked', [SuperAuthController::class,'blocked'])->name('super.blocked');

    // Route::get('forgot-password', 'AdminAuthController@showForgetPassword')->name('cms.admin.forgot_password_view');
    // Route::post('forgot-password', 'AdminAuthController@requestNewPassword')->name('cms.admin.forgot_password');
});

Route::get('/test', function () {
    App::setLocale('en');
    dd(App::getLocale());
});

