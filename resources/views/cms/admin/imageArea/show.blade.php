<link rel="stylesheet" href="{{asset('<link rel="stylesheet" href="../assets/vendor/dropify/css/dropify.min.css">
')}}">

  <!-- Area Image New  -->
        <div id="generalImage" class="modal fade new-project-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
            <form action="{{route('area.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
           {{-- @if($area) <div>{{$area->image}}</div> @endif --}}
                    <div class="modal-content">

                        <div class="modal-body">

                           <input @if($area)  data-default-file="{{ url('images/generalimages/' . $area->image) }}" @endif type="file" name="image" class="dropify">


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-round btn-default" data-dismiss="modal">إغلاق</button>
                            <button type="submit" class="btn btn-round btn-success">إنشاء</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{-- //////////////// --}}
        <script src="{{asset('cms/assets/vendor/jquery-steps/jquery.steps.js')}}"></script><!-- JQuery Steps Plugin Js -->
        <script src="{{asset('cms/html/assets/js/pages/forms/dropify.js')}}"></script>
    <script src="{{asset('cms/html/assets/js/pages/forms/form-wizard.js')}}"></script>
    <script src="{{asset('cms/assets/vendor/dropify/js/dropify.js')}}"></script>
