@extends('cms.admin.parent')
@section('content')



    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashbord') }}"
                                        style="font-size: 20px">الرئيسية</a></li>
                                {{-- <li class="breadcrumb-item"><a
                                        href="{{ route('center.index', app()->getLocale()) }}"
                                        style="font-size: 20px">{{ __('parent.center') }}</a></li>
                                --}}
                                <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">
                                    التنوع الحيوي
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        {{-- <a
                            href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507"
                            class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                        --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                            <form method="POST" action="{{ route('brid.store') }}"
                                enctype="multipart/form-data">


                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </div>
                                @endif
                                @if (session()->has('message'))
                                    <div class="alert {{ session()->get('status') }} alert-dismissible fade show"
                                        role="alert">
                                        <span> {{ session()->get('message') }}</span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-12 col-sm-10">
                                        {{-- الرؤية بالإنجليزية
                                        --}}
                                        <div class="form-group">
                                            <label style="font-size: 20px"> المحتوى - عربي</label>

                                            <textarea name="descar" rows="4" class="form-control no-resize"
                                                >@if ($brid){{ $brid->descar }} @endif </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px">المحتوى -إنجليزي</label>

                                            <textarea name="descen" rows="4" class="form-control no-resize"
                                              >@if ($brid){{ $brid->descen }}@endif</textarea>
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px">عدد الفصائل النباتية</label>
                                            <input type="number" class="form-control" @if($brid) value={{ $brid->countplant }}@endif
                                                style="font-size: 20px" name="countplant"
                                                >
                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px">عدد فصائل الطيور</label>
                                            <input type="number" class="form-control" @if($brid) value={{ $brid->countbrid }}@endif
                                                style="font-size: 20px" name="countbrid"
                                                >
                                        </div>
                                           <div class="form-group">
                                            <label style="font-size: 20px">عدد فصائل الثديات</label>
                                            <input type="number" class="form-control" @if($brid) value={{ $brid->countmammal }}@endif
                                                style="font-size: 20px" name="countmammal"
                                                >
                                        </div>

                                        {{-- الرسلة بالإنجليزية
                                        --}}







                                    </div>
                                </div>
                                <div>
                                    <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit">
                                        حفظ
                                    </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>

@endsection
