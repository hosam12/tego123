@extends('cms.admin.parent')
@section('lang')
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item pt-2 pb-2" href="{{ route(
                                            request()->route()->getName(),
                                            ['en', 1],
                                        ) }}"><img src="{{ asset('cms/assets/images/flag/us.svg') }} "
                class="w20 mr-2 rounded-circle"> US English</a>


        <a class="dropdown-item pt-2 pb-2" href="{{ route(
                                            request()->route()->getName(),
                                            ['ar', 1],
                                        ) }}"><img src="{{ asset('cms/assets/images/flag/arabia.svg') }} "
                class="w20 mr-2 rounded-circle"> العربية</a>

    </div>
@endsection

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashbord') }}"
                                        style="font-size: 20px">الرئيسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('super.create') }}"
                                        style="font-size: 20px">أدمن جديد</a></li>
                                <li class="breadcrumb-item " style="font-size: 20px" aria-current="page">
                                   الأدمن
                                </li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">{{ __('parent.super') }}</h2>

                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size: 17px">الصورة</th>
                                        <th style="font-size: 17px"> الإسم</th>
                                        <th style="font-size: 17px">الإيميل </th>
                                        {{-- <th style="font-size: 17px">{{ __('general.show') }} </th> --}}
                                        <th style="font-size: 17px">الحالة</th>
                                        <th style="font-size: 17px">تاريخ الإنشاء</th>
                                        <th style="font-size: 17px">الإعدادات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <span hidden>{{ $i = 0 }}</span>
                                    @foreach ($supers as $item)
                                        <span hidden>{{ $i++ }}</span>

                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td class="w60">
                                                <img src="{{ url('images/supers/' . $item->image) }}"
                                                    data-toggle="tooltip" data-placement="top" title="{{ $item->name }}"
                                                    alt="Avatar" class="w35 h35 rounded">
                                            </td>



                                                <td><span style="font-size: 17px">{{ $item->name }}</span></td>
                                                  <td><span style="font-size: 17px">{{ $item->email }}</span></td>



                                            {{-- <td>
                                                <a class="btn btn-info btn-sm"
                                                    href="{{ route('super.show', [app()->getLocale(), $item->id]) }}">
                                                    <i class="icon-note">
                                                    </i>
                                                    {{ __('general.show') }}
                                                </a>
                                                <span class="badge badge-dark"></span>
                                            </td> --}}
                                            <td>
                                                @if ($item->status == 'Active')
                                                    <span style="font-size: 17px"
                                                        class="badge badge-success">فعّال</span>
                                                @else
                                                    <span style="font-size: 17px"
                                                        class="badge badge-danger">متوقفة</span>
                                                @endif

                                            </td>
                                            <td><span
                                                    style="font-size: 17px">{{ $item->created_at->format('Y.m.d') }}</span>
                                            </td>

                                            <td>
                                                <a href="{{ route('super.edit',  $item->id) }}"
                                                    type="button" style="font-size: 20px" class="btn btn-sm btn-default"
                                                    title="تعديل"><i class="fa fa-edit"></i>
                                                    تعديل</a>
                                                <a onclick="confirmDelete(this, '{{ $item->id }}')" type="button"
                                                    style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert"
                                                    title=" حذف" data-type="confirm"><i
                                                        class="fa fa-trash-o text-danger">
                                                       حذف </i></a>


                                            </td>




                                            {{-- <td>
                                                <a href="javascript:void(0);" class="btn btn-info btn-round">Interview</a>

                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {

            Swal.fire({
                title: 'هل انت متأكد من الحذف ؟',
                text: "لن تكون قادرا على استرجاعها",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'تراجع'
            }).then((result) => {
                if (result.value) {
                    deletecategory(app, id, )
                }
            })
        }

        function deletecategory(app, id) {
            axios.delete('/cms/admin/super/' + id)
                .then(function(response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function(error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function() {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }

    </script>

@endsection
