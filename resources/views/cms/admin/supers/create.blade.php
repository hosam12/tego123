@extends('cms.admin.parent')
@section('lang')
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item pt-2 pb-2" href="{{ route(
                                            request()->route()->getName(),
                                            ['en', 1],
                                        ) }}"><img src="{{ asset('cms/assets/images/flag/us.svg') }} "
                class="w20 mr-2 rounded-circle"> US English</a>


        <a class="dropdown-item pt-2 pb-2" href="{{ route(
                                            request()->route()->getName(),
                                            ['ar', 1],
                                        ) }}"><img src="{{ asset('cms/assets/images/flag/arabia.svg') }} "
                class="w20 mr-2 rounded-circle"> العربية</a>

    </div>
@endsection
@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('super.index') }}"
                                style="font-size: 20px">الأدمن</a></li>
                                <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">أدمن جديد
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        {{-- <a
                            href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507"
                            class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a>
                        --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                            <form method="POST" action="{{ route('super.store') }}" enctype="multipart/form-data">


                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </div>
                                @endif
                                @if (session()->has('message'))
                                    <div class="alert {{ session()->get('status') }} alert-dismissible fade show"
                                        role="alert">
                                        <span> {{ session()->get('message') }}</span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                        <label style="font-size: 20px">الإسم</label>
                                            <input type="text" class="form-control" value="{{ old('name') }}"
                                                style="font-size: 20px" name="name" >


                                        </div>
                                                              <div class="form-group">
                                                              <label style="font-size: 20px">الإيميل</label>
                                            <input type="text" class="form-control" value="{{ old('email') }}"
                                                style="font-size: 20px" name="email">


                                        </div>
                                                              <div class="form-group">
                                                              <label style="font-size: 20px">كلمة السر</label>
                                            <input type="password" class="form-control"
                                                style="font-size: 20px" name="password" >


                                        </div>
                                                              <div class="form-group">
                                            <label style="font-size: 20px">تأكيد كلمة السر</label>
                                            <input type="password" class="form-control"
                                                style="font-size: 20px" name="confirm" >


                                        </div>


                                        <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <label style="font-size: 20px"> الصورة </label>
                                                <input type="file" width="590px" height="390px" class="dropify"
                                                    name="image">
                                            </div>

                                        </div>



                                        <div class="form-group">
                                            <label style="font-size: 20px">الحالة</label>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" name="status"
                                                    id="status" @if (old('status') == 'on')
                                                checked @endif>
                                                <label class="custom-control-label" style="font-size: 20px"
                                                    for="status">مُفعلة</label>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div>
                                    <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> حفظ
                                    </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>

@endsection
