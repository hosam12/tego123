@extends('cms.admin.parent')



@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashbord') }}"
                                        style="font-size: 20px">الرئيسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('village.index') }}"
                                        style="font-size: 20px">القرى والبلدات</a></li>
                                <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">
                                    مشاهدة
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{ route('village.edit',$village->id) }}"
                            class="btn btn-sm btn-primary btn-round" onclick="ss()" title="">
                            تعديل</a>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">

                    <div class="card">
                        <div class="mail-inbox">

                            <div class="mobile-left">
                                <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i
                                        class="fa fa-bars"></i></a>
                            </div>

                            <div class="body mail-right check-all-parent">
                                <div class="mail-detail-full">

                                    <div class="card n_category">
                                        <img width="100%" height="400px" class="card-img-top"
                                            src="{{ url('images/villages/' . $village->image) }}" alt="Card image cap">

                                        {{-- <h5 class="card-title"><a
                                                href="javascript:void(0);">{{ $center->title }}</a></h5>
                                        <p class="card-text">{{ $center->describe }}</p> --}}


                                    </div>

                                        <h5 class="modal-title" id="exampleModalLabel">{{ $village->namear }}</h5>
                                        <div class="modal-body">
                                            <p>{{ $village->descar }}</p><br>
                                            </div>
                                       <hr>
                                       <br>
                                            <h5 class="modal-title" id="exampleModalLabel">{{ $village->nameen }}</h5>
                                            <div class="modal-body">
                                                <p>{{ $village->descen }}</p><br>
                                            </div>



                                    @if ($village == 'Visible')
                                        <span style="font-size: 17px"
                                            class="badge badge-success">مُفعلة</span>
                                    @else
                                        <span style="font-size: 17px"
                                            class="badge badge-danger">مُطفئة</span>
                                    @endif

                                </div>


                                <hr>
                                <p>تاريخ الإنشاء <strong> __
                                        {{ $village->created_at->format('Y.m.d') }}</strong></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
