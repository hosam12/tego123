@extends('cms.admin.parent')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashbord') }}"
                                        style="font-size: 20px">الرئيسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('center.index') }}"
                                        style="font-size: 20px">المركز الإعلامي</a></li>
                                <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">
                                    خبر جديد
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{ route('center.show',$center->id) }}"
                            class="btn btn-sm btn-primary btn-round" onclick="ss()" title="">
                            عرض</a>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                            <form method="POST" action="{{ route('center.update', $center->id]) }}"
                                enctype="multipart/form-data">

                                @method("PUT")
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </div>
                                @endif
                                @if (session()->has('message'))
                                    <div class="alert {{ session()->get('status') }} alert-dismissible fade show"
                                        role="alert">
                                        <span> {{ session()->get('message') }}</span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                            <label style="font-size: 20px">العنوان - عربي</label>
                                            <input type="text" class="form-control" value="{{ $center->titlear }}"
                                                style="font-size: 20px" name="titlear"
                                               >


                                        </div>

                                        <div class="form-group">
                                            <label style="font-size: 20px">العنوان - إنجليزي</label>
                                            <input type="text" class="form-control" value="{{ $center->titleen }}"
                                                style="font-size: 20px" name="titleen"
                                               >


                                        </div>


                                        <div class="row clearfix">



                                            <div class="col-lg-12">
                                                <label style="font-size: 20px">الصورة</label>

                                                <input type="file" width="590px" height="390px" class="dropify"
                                                    data-default-file="{{ url('images/centers/' . $center->main_image) }}"
                                                    name="main_image">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px">التفاصيل - عربي</label>

                                            <textarea name="describear" rows="4" class="form-control no-resize"
                                                >{{ $center->describear }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label style="font-size: 20px">التفاصيل - إنجليزي</label>

                                            <textarea name="describeen" rows="4" class="form-control no-resize"
                                              >{{ $center->describeen }}</textarea>
                                        </div>


                                        <div class="form-group">
                                            <label style="font-size: 20px">حالة العرض</label>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" name="status"
                                                    id="status" @if ($center->status == 'Visible')
                                                checked @endif>
                                                <label class="custom-control-label" style="font-size: 20px"
                                                    for="status">مُفعلة</label>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div>
                                    <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit">
                                        {{ __('general.edit') }}
                                    </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>

@endsection
