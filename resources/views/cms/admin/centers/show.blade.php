@extends('cms.admin.parent')

@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashbord') }}"
                                        style="font-size: 20px">الرئيسية</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('center.index') }}"
                                        style="font-size: 20px">المركز الإعلامي</a></li>
                                <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">
                                    عرض
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{ route('center.edit', $center->id) }}"
                            class="btn btn-sm btn-primary btn-round" onclick="ss()" title="">
                            تعديل</a>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">

                    <div class="card">
                        <div class="mail-inbox">

                            <div class="mobile-left">
                                <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i
                                        class="fa fa-bars"></i></a>
                            </div>

                            <div class="body mail-right check-all-parent">
                                <div class="mail-detail-full">

                                    <div class="card n_category">
                                        <img width="100%" height="400px" class="card-img-top"
                                            src="{{ url('images/centers/' . $center->main_image) }}" alt="Card image cap">

                                        {{-- <h5 class="card-title"><a
                                                href="javascript:void(0);">{{ $center->title }}</a></h5>
                                        <p class="card-text">{{ $center->describe }}</p> --}}


                                    </div>

                                    <h5 class="modal-title" id="exampleModalLabel">
                                        {{  $center->titlear }}</h5>
                                    <div class="modal-body">
                                        <p>{{  $center->describear  }}</p>
                                    </div>
                                    <hr>
                                      <h5 class="modal-title" id="exampleModalLabel">
                                        {{  $center->titleen }}</h5>
                                    <div class="modal-body">
                                        <p>{{  $center->describeen  }}</p>
                                    </div>

                                    @if ($center == 'Visible')
                                        <span style="font-size: 17px"
                                            class="badge badge-success">مُفعل</span>
                                    @else
                                        <span style="font-size: 17px"
                                            class="badge badge-danger">مطفئ</span>
                                    @endif


                                    <hr>
                                    <p>تاريخ الإنشاء <strong> __
                                            {{ $center->created_at->format('Y.m.d') }}</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
