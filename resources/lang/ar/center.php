<?php
return[
    'titletable'=>'العنوان',
    'desctable'=>'الوصف',
    'mainimage'=>'الصورة العامة',
    'titlear'=>'العنوان (عربي) ',
    'descar'=>'الوصف (عربي)',
    'titleen'=>'العنوان (إنجليزي)',
    'descen'=>'الوصف (إنجليزي)',
    'status'=>'حالة العرض',
    'settings'=>'الإعدادت',
    'turn_on'=>'فعّال',
    'turn_off'=>'متوقفة',
    'plashedtitlear'=>'العنوان-عربي',
    'plashedtitleen'=>'العنوان- إنجليزي',
    'plasheddescar'=>'الوصف -عربي',
    'plasheddescen'=>'الوصف - إنجليزي',
];
