<?php
return[

    'namear'=>' (عربي) اسم البلدة',
    'descar'=>'(عربي) وصف البلدة',
    'nameen'=>' (إنجليزي) اسم البلدة',
    'descen'=>'(إنجليزي) وصف البلدة',
    'village'=>'القرى والبلدات',
    'image'=>'صورة البلدة',
    'index'=>'جميع القرى',
    'create'=>'قرية جديدة',
    'pashnamear'=>'اسم البلدة - عربي',
    'pashnameen'=>'  اسم البلدة - إنجليزي',
    'nametabel'=>'اسم البلدة',
     'desctabel'=>'الوصف',
    'pashdescar'=>' الوصف - عربي',
    'pashdescen'=>'   الوصف - إنجليزي',
    'generalimage'=>'الصورة العامة',

    // 'village'



];
