<?php

return [

    'center'=>'المركز الإعلامي',
    'category'=>'السياحة الداخلية',
    'categoryindex'=>'جميع الفئات',
    'categorycreate'=>'فئة جديدة',
    'centercreate'=>'خبر جديد',
    'centerindex'=>'جميع الأخبار',
      'centeredit'=>'تعديل',
    'centershow'=>'عرض',
    /////////
    'contact'=>'التواصل',
    'message'=>'الرسائل',
    'mymessage'=>'رسالتنا',
    'about_as'=>'النبذة ',
    'village'=>'القرى والبلدات',
    'brid'=>'التنوع الحيوي',
     'super'=>'الأدمن',
     'superindex'=>'جميع الأدمن',



];
