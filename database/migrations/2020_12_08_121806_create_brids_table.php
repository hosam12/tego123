<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBridsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brids', function (Blueprint $table) {
            $table->id();
            $table->string('descar')->nullable();
            $table->string('descen')->nullable();
            $table->integer('countplant')->nullable();
            $table->integer('countbrid')->nullable();
            $table->integer('countmammal')->nullable();
            $table->string('titlear')->nullable();
            $table->string('titleen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brids');
    }
}
