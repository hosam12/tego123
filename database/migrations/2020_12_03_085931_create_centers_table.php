<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centers', function (Blueprint $table) {
            $table->id();
            $table->string('titleen');
            $table->string('main_image');
            $table->string('describeen')->nullable();
            $table->string('describear')->nullable();
            $table->string('longdescar')->nullable();
            $table->string('longdescen')->nullable();
              $table->string('titlear');
            $table->enum('status',['Visible','InVisible'])->default('Visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centers');
    }
}
